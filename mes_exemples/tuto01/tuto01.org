#+LINK: Lab1 http://fredtantini.free.fr/Lab/mes_exemples/tuto01/index%s.html


On commence donc tout doucement avec quelques propriétés de l’objet =Game=.

D’abord, récupérez les exemples [[bitbucket][ici]] si vous voulez modifier des
valeurs, ça évitera de tout recopier. Si vous tapez tout vous-même,
dans le fichier html, il ne faut pas oublier les liens vers enchant.js
et vers le script du jeu :
#+BEGIN_SRC html
    <script type="text/javascript" src="../lib/enchant.js"></script>
    <script type="text/javascript" src="main1.js"></script>
#+END_SRC

Voici le [[Lab1:1][premier exemple]], commenté :

#+BEGIN_SRC javascript -n
enchant();

HEIGHT = 320;
WIDTH = 120;

window.onload = function() {
    var game = new Game(WIDTH, HEIGHT);

    game.preload(['../images/icon0.gif']); //les images à précharger,
                                           //mais bon là il n’y en a pas
    game.onload = function() { // le code du jeu
        var label = new Label(''); //on va afficher le numéro de frame
        game.rootScene.addChild(label);//on verra dans un autre
                                       //exemple ce que ça fait, même
                                       //si ça se devine
        game.rootScene.backgroundColor='grey';//pour bien voir le canvas
        game.rootScene.addEventListener('enterframe',function() {
            label.text = game.frame; //à chaque frame on met le label à jour
        });
    }
    game.fps = 24; //le nombre de frames par secondes
    game.start();
}
#+END_SRC

Comme on l’a vu dans l’intro, on passe à =Game= une largeur et une
hauteur. On fait des préchargements (inutiles ici), puis on ajoute le
code dans le =.onload=. La nouveauté ici est le =game.fps = 24;=.
C’est comme cela que l’on fixe le nombre de frames par secondes. et
ensuite, on lance le jeu avec =game.start()=. 

Avant de continuer, si vous essayez cet exemple, vous verrez qu’il se
met en plein écran. Et du coup, la résolution n’est pas du tout en
120×320 !

Pour résoudre ce problème, on peut préciser qu’il faut le mettre à
l’échelle 1:1 en ajoutant un =game.scale = 1=. 

Dans le [[Lab1:2][second exemple]], on ajoute un =Sprite= vide, on modifie le
nombre de fps, et on change d’échelle :
#+BEGIN_SRC javascript -n
enchant();

HEIGHT = 320;
WIDTH = 120;

window.onload = function() {
    var game = new Game(WIDTH, HEIGHT);
    game.preload(['../images/icon0.gif']); //les images à précharger,
                                           //mais bon là il n’y en a pas
    game.onload = function() { // le code du jeu
        var label = new Label(''); //on va afficher le numéro de frame
        game.rootScene.addChild(label);//on verra dans un autre
                                       //exemple ce que ça fait, même
                                       //si ça se devine
        game.rootScene.backgroundColor='grey';//pour bien voir le canvas
        game.rootScene.addEventListener('enterframe',function() {
            label.text = game.frame; //à chaque frame on met le label à jour
        });
        game.rootScene.addChild(new Sprite(32,32));
    }
    game.fps = 4; //le nombre de frames par secondes
    game.scale = 2; //mise à l’échelle
    game.debug();
}
#+END_SRC

Si vous lancez le script, vous verrez qu’effectivement, le numéro de
frame défile bien moins rapidement, et que le canvas n’est plus
maximisé.

Autre nouveauté, les rectangles rouges. Ils sont là parce qu’à la
place d’avoir mis à la fin =game.start()= on a fait un =game.debug()=.
On verra par la suite que ça peut être bien pratique d’avoir la
délimitation des éléments.

Dans votre navigateur, faites ‰F12‰, puis allez sur l’onglet Console.
Là, tapez ‰window.Game.instance.pause()‰ et vous verrez le nombre de
frames se figer. Pour repartir, on peut utiliser =.resume()=, ou
=.start()= (=.debug()=) si on veut repartir de zéro. La console du
navigateur est bien pratique pour déboguer une fois qu’on a mis le jeu
en pause. Une autre façon de faire et de mettre des
=console.log("hello");= dans le script du jeu.

Et pour finir, pour accéder plus facilement à vos variables, vous
pouvez supprimer le =var= lors de la déclaration, donc =label = new
Label('');= ou =game = new Game(10,10);=. En faisant cela, la variable
devient globale et est accessible en dehors du =window.onload=. On
peut donc la déclarer la ligne avant avec un =var game=null;= par
exemple :

#+BEGIN_SRC javascript
var game = null;

window.onload = function() {
    game = new Game(WIDTH, HEIGHT);
#+END_SRC


Dans le [[file:c:/Users/ftantini/Documents/Divers/Vrac/enchant/mes_exemples/tuto02/tuto02.org][prochain article]], on verra les =Scene= et je parlerai
brièvement de =addEventListener=.
