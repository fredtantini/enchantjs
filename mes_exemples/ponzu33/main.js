enchant();
window.onload = function() {
    game = new Game(320, 320);
    game.fps = 20;
    game.preload(['chara4.png']);
    game.onload = function() {
        var info = new Label('');
        game.rootScene.addChild(info);
        bear = new Sprite(32,32);  
        bear.x = 0;                 
        bear.y = 240;               
        bear.width = 20;//32;
        bear.height = 32;
        bear.image = game.assets['chara4.png']; 
        game.rootScene.addChild(bear);
        bear._frameLeft = 6;

        

        game.rootScene.addEventListener('touchstart', function(e){
            bear.x = e.localX
        });
        game.rootScene.addEventListener('touchmove', function(e){
            bear.x = e.localX
        });
        game.score = 0;
        game.rootScene.addEventListener('enterframe',function(){
            info.text = game.score +" "+game.rootScene;
            if(game.frame % 24 == 0){
                addEnemy();
            }
        });
    }
    game.debug();
}
function addEnemy(pos){
    var enemy = new Sprite(20,32);    
    enemy.x = rand(300);//320 - 20          
    enemy.y = 0;
    enemy.image = game.assets['chara4.png'];
    enemy.frame = 1 + rand(3);
    enemy.width = 20;
    enemy._frameLeft = 6 + (32 * (enemy.frame));
    enemy.addEventListener('enterframe', function(e) {
        if(this.y >300){game.score++;game.rootScene.removeChild(this);}
        if(this.intersect(bear)){       
                game.end(game.score, "SCORE: " + game.score);
        }else{
            this.y += 3;                
        }
    });
    game.rootScene.addChild(enemy);
}
function rand(num){
    return Math.floor(Math.random() * num);
}
