enchant();

window.onload = function() {
    game = new Game(320, 320);
    game.fps = 20;
    game.preload(['chara4.png']);
    //プリロードする画像を相対パスで指定

    game.onload = function() {
    // プリロード終了後に呼ばれる関数を指定する

        var info = new Label('');
        game.rootScene.addChild(info);

        // ここから、クマのキャラクターを表示する処理
        bear = new Sprite(32, 32);  // 32x32サイズの Sprite オブジェクトを生成
        bear.x = 0;                 // Sprite の左上のx座標を指定
        bear.y = 240;               // Sprite の左上のy座標を指定
        bear.width = 20;//32;
        bear.height = 32;
        bear.image = game.assets['chara4.png']; // 画像を指定
        bear._frameLeft = 6;
        // 「chara1.gif」を32x32の格子で切り取ったのち、0番目(=左上)のものを用いる
        // ゲーム中に frame の値を操作することで、アニメーションを表現できる

/*        background = new Sprite(320, 320);  // 320x320 サイズの Sprite オブジェクトを生成
        background.x = background.y = 0;    // Sprite の左上の x, y 座標を指定
        background.image = game.assets['bg.png'] // bg.png を指定
*/
        // タッチしたときにクマを移動させる
        game.rootScene.addEventListener('touchstart', function(e){
            bear.x = e.localX
        });

        // タッチ座標が動いたときにクマを移動させる
        game.rootScene.addEventListener('touchmove', function(e){
            bear.x = e.localX
        });

        game.score = 0;

        game.rootScene.addEventListener('enterframe',function(){
            info.text = game.score +" "+game.rootScene;
            if(game.frame % 24 === 0){
                // 6フレームごとにバナナを増やす関数を実行
                addEnemy();
            }
        });

     //   game.rootScene.addChild(background);
        game.rootScene.addChild(bear);

    }
    game.debug();
    // プリロードをスタート
}

// バナナを増やす関数 (6フレームごとに呼ばれる)
function addEnemy(pos){
    var enemy = new Sprite(20,32);    // Spriteを生成
    enemy.x = rand(300);               // 0 から 319 -20 のあいだの乱数
    enemy.y = 0;
    enemy.image = game.assets['chara4.png'];
    enemy.frame = 1+rand(3);
    enemy.width = 20;
    enemy._frameLeft = 6 + (32 * (enemy.frame));
    enemy.addEventListener('enterframe', function(e) {
        if(this.y >300){game.score++;game.rootScene.removeChild(this);}
        
        if(this.intersect(bear)){       // bearとの当たり判定
                game.end(game.score, "SCORE: " + game.score);
        }else{
            this.y += 3;                // y座標を増やす (落下)
        }
    });
    game.rootScene.addChild(enemy);
    // バナナを画面に追加
}

// 引数 num を受け取って、0 から (num - 1) までの乱数を返す関数
function rand(num){
    return Math.floor(Math.random() * num);
}
