enchant();

HEIGHT = 320;
WIDTH = 320;
CAR_SPEED = WIDTH / 20;
ITEM_SPEED = HEIGHT/100;

window.onload = function() {
    game = new Game(HEIGHT, HEIGHT);
    game.fps = 24;
    game.preload(['chara1.gif','icon0.gif','bg.png']);

    game.onload = function() {
        //on crée la voiture
        someCar = new Car();
        
        game.rootScene.addEventListener('touchstart', function(e){
            //À chaque appui, on change la voiture de côté si elle ne bouge pas
            if (someCar.moving == 0) {
                //si à gauche, moving devient 1
                //si à droite, moving devient -1
                someCar.moving = (-2 * someCar.position) + 1;
            }
        });
        
        game.score = 0;
        var label = new Label('');
        game.rootScene.addChild(label);
        
        game.rootScene.addEventListener('enterframe',function() {
            label.text = game.score;
            if(game.frame % (5*CAR_SPEED) == 0){
                new Item();
            }
            if(game.rootScene.age > game.fps * 20){
       //         game.end(game.score, game.score + " 本のバナナを取りました!");
                
            }
        });


    }
    game.start();
    
}

Car = enchant.Class.create(Sprite, {
    initialize: function() {
        var game = enchant.Game.instance;
        Sprite.call(this, 32, 32);
        
        this.x = (WIDTH/4) - 16;
        this.xmin = this.x;
        this.xmax = (3*WIDTH/4) - 16;
        this.y = 240;               
        this.image = game.assets['chara1.gif']; 
        this.frame = 0;
        this.position = 0; // 0 si gauche, 1 pour droite
        this.moving = 0;

        this.addEventListener('enterframe', function(e){
            if (this.moving != 0) {
                this.x += this.moving*5; //on déplace de 5
                if ((this.moving == -1) && (this.x <= this.xmin)) {
                    this.position = 0;
                    this.x = this.xmin;
                    this.moving = 0;                
                }
                if ((this.moving == 1) && (this.x>=this.xmax)) {
                    this.position = 1;
                    this.x = this.xmax;
                    this.moving = 0;                
                }
            }
        });
        game.rootScene.addChild(this);
    }
});


Item = enchant.Class.create(Sprite, {
    initialize: function() {
        var game = enchant.Game.instance;
        Sprite.call(this, WIDTH/2, 10);
  
        this.y = 0;
        this.x =  (rand(2)*WIDTH/2) ;
        
	var surface = new Surface(WIDTH/2, 10);
        surface.context.lineWidth = 5;
        surface.context.rect(0, 0, WIDTH/2, 10);
        surface.context.fillStyle = "rgb(150, 150, 150)";
        surface.context.fill();
        surface.context.stroke();
	this.image = surface;
        

        this.addEventListener('enterframe', function() {
            this.y += ITEM_SPEED;
            if (this.y > 320) {
                this.scene.removeChild(this);
                game.score++;
            }
        });

        this.addEventListener('enterframe', function(e) {                
            if (this.intersect(someCar)) {
                game.score--;
                game.rootScene.removeChild(this);
            }
        });

        
        game.rootScene.addChild(this);


        
    }
});


function rand(num){
    return Math.floor(Math.random() * num);
}


