enchant();

HEIGHT = 320;
WIDTH = 320;
N_CARS = 4;
NOMBRE_FRAMES_MAX = 4;
LANE_WIDTH = WIDTH / N_CARS;
SPEED = LANE_WIDTH / 20;

window.onload = function() {
    game = new Game(HEIGHT, HEIGHT);
    game.fps = 24;
    game.preload(['chara1.gif','icon0.gif','bg.png']);
    

    game.onload = function() {
        //on crée les voitures, chacune sur sa ligne
        game.arr_cars = [];
        for(var i_car = 0; i_car < N_CARS; i_car++) {
            game.arr_cars.push(new Car(i_car));
            
        }
        
    
        game.rootScene.addEventListener('touchstart', function(e){
            //À chaque appui, on change la voiture de côté si elle ne bouge pas
            var someCar = game.arr_cars[Math.floor(e.localX/LANE_WIDTH)];
            if (someCar.moving == 0) {
                //si à gauche, moving devient 1
                //si à droite, moving devient -1
                someCar.moving = (-2 * someCar.position) + 1;
            }
        });
        game.score = 0;

        game.rootScene.addEventListener('enterframe',function() {
            if(game.frame % SPEED == 0){
                addItem(rand(N_CARS), rand(2));
            }
            if(game.rootScene.age > game.fps * 20){
       //         game.end(game.score, game.score + " 本のバナナを取りました!");
                
            }
        });


    }
    game.debug();
    
}

Car = enchant.Class.create(Sprite, {
    initialize: function(lane) {
        var game = enchant.Game.instance;
        Sprite.call(this, 32, 32);
        
        this.lane = lane;
        this.x = lane * LANE_WIDTH;
        this.xmin = this.x;
        this.xmax = (lane+1) * LANE_WIDTH - 32;
        this.y = 240;               
        this.image = game.assets['chara1.gif']; 
        this.frame = lane % NOMBRE_FRAMES_MAX;
        this.position = 0; // 0 si gauche, 1 pour droite
        this.moving = 0;

        this.addEventListener('enterframe', function(e){
            if (this.moving != 0) {
                console.log('moving: '+this.moving);
                console.log('position: '+this.position);
                console.log('x: '+this.x);
                this.x += this.moving*5; //on déplace de 5

                if ((this.moving == -1) && (this.x <= this.xmin)) {
                    this.position = 0;
                    this.x = this.xmin;
                    this.moving = 0;                
                }
                if ((this.moving == 1) && (this.x>=this.xmax)) {
                    this.position = 1;
                    this.x = this.xmax;
                    this.moving = 0;                
                    
                }
                
            }
            
        });
        game.rootScene.addChild(this);
    }
});

/*


function addCar(lane) {
    
    var theCar = new Sprite(32, 32);
    // commence sur la gauche
    theCar.lane = lane;
    theCar.x = lane * LANE_WIDTH;
    theCar.xmin = theCar.x;
    theCar.xmax = (lane+1) * LANE_WIDTH - 32;
    theCar.y = 240;               
    theCar.image = game.assets['chara1.gif']; 
    theCar.frame = lane % NOMBRE_FRAMES_MAX;
    theCar.position = 0; // 0 si gauche, 1 pour droite
    theCar.moving = 0;

    theCar.addEventListener('enterframe', function(e){
        if (this.moving != 0) {
            console.log('moving: '+this.moving);
                        console.log('position: '+this.position);
            console.log('x: '+this.x);
            this.x += this.moving*5; //on déplace de 5

            if ((this.moving == -1) && (this.x <= this.xmin)) {
                this.position = 0;
                this.x = this.xmin;
                this.moving = 0;                
            }
            if ((this.moving == 1) && (this.x>=this.xmax)) {
                this.position = 1;
                this.x = this.xmax;
                this.moving = 0;                
                
            }
                
        }
        
    });
    
    game.rootScene.addChild(theCar);
    return theCar;

}
*/
function addItem(lane, typeOfItem){
    
    var item = new Sprite(16, 16);    
    item.x = rand(320);               
    item.y = 0;

    //bonus
    if (typeOfItem == 0) {
        item.image = game.assets['icon0.gif'];
        item.frame = 16;
        item.addEventListener('enterframe', function(e) {
/*            var set = this.intersect(
            if(this.intersect(leftCar)){       
                game.rootScene.removeChild(this); 
                game.score ++;                    
            }else{
                this.y += 3;                
            } */
        });


    }
    
    //malus
    if (typeOfItem == 1) {
        item.image = game.assets['icon0.gif'];
        item.frame = 15;
    }
        
    game.rootScene.addChild(item);
    
}


function rand(num){
    return Math.floor(Math.random() * num);
}
